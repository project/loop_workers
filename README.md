INTRODUCTION
------------

The Loop Workers module allows developers to define workers that continually
process a loop of items. This is a similar concept to Drupal's QueueAPI, but
instead of a queue that items are put into to be processed, a loop is a defined
list which the worker continually works over, returning to the start
automatically when it reaches the end.

One use case might be a site which needs to check the body field of all nodes to
ensure that no external links are broken.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

Ultimate Cron module (https://www.drupal.org/project/ultimate_cron) may
optionally be used to run loop workers.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

To define a loop, create a LoopWorker plugin. This may either:

1. Return the whole loop's list in one method.
2. Return a partial list. Use this when the whole list would be too large to
   hold in memory.

If the entire loop list is given, it is cached by the system. Otherwise, only
the count is cached. In both cases, the plugin may define cache tags for this
data, to allow the system to get an updated version of the list from the plugin.

The list of items returned by the plugin must satisfy these properties in order
to ensure the list is processed in a consistent order:

1. The keys of the array must be consistent. That is, a particular key must
   always refer to the same item in subsequent calls to get the list. In
   particular, beware of using the result of an entity query to get a list: for
   revisionable entities, the keys of the result of an entity query are revision
   IDs, which would change.
2. The sort order must be consistent. That is, two items in the list must always
   have the same relative order, even if items are added or removed between
   them.

See the test module for examples.
