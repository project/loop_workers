<?php

/**
 * @file
 * Hooks provided by the Loop workers module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Perform alterations on Loop Rate definitions.
 *
 * @param array $info
 *   Array of information on Loop Rate plugins.
 */
function hook_loop_rate_info_alter(array &$info) {
  // Change the class of the 'foo' plugin.
  $info['foo']['class'] = SomeOtherClass::class;
}

/**
 * Perform alterations on Loop Worker definitions.
 *
 * @param array $info
 *   Array of information on Loop Worker plugins.
 */
function hook_loop_worker_info_alter(array &$info) {
  // Change the class of the 'foo' plugin.
  $info['foo']['class'] = SomeOtherClass::class;
}

/**
 * @} End of "addtogroup hooks".
 */
