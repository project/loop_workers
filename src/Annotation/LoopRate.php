<?php

namespace Drupal\loop_workers\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Loop Rate plugin annotation object.
 *
 * Plugin namespace: LoopRate.
 *
 * @Annotation
 */
class LoopRate extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id = '';

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label = '';

}
