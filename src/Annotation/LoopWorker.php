<?php

namespace Drupal\loop_workers\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Loop Worker plugin annotation object.
 *
 * Plugin namespace: LoopWorker.
 *
 * @Annotation
 */
class LoopWorker extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id = '';

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label = '';

  /**
   * The way in which this loop is run.
   *
   * This module supports the following values:
   *  - 'hook_cron': The loop is processed by core's cron via this modules's
   *    hook_cron() implementation.
   *  - 'ultimate_cron': This module provides an Ultimate Cron job entity for
   *    the loop plugin. Requires ultimate_cron module to be present.
   *
   * Other values will be ignored by this module.
   *
   * Use \Drupal\loop_workers\LoopWorkerManager::getPluginDefinitionsByRunner()
   * to get all plugin definitions with a particular value for this property.
   *
   * @var string
   */
  public $runner = '';

  /**
   * The rate plugin for this loop, and its configuration.
   *
   * This array must contain the 'type' key, whose value must be the ID of a
   * loop rate plugin.
   *
   * All other values in this array are passed to the loop rate plugin as its
   * configuration.
   *
   * @var array
   */
  public $rate = [];

}
