<?php

namespace Drupal\loop_workers\Exception;

/**
 * Exception class to throw to indicate that a loop should be skipped.
 *
 * An implementation of
 * \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface::processItem()
 * throws this class of exception to indicate that processing of the whole loop
 * should be skipped. This should be thrown rather than a normal Exception if
 * the problem encountered by the loop worker is such that it can be deduced
 * that workers of subsequent items would encounter it too. For example, if a
 * remote site that the loop worker depends on appears to be inaccessible.
 */
class SuspendLoopException extends \RuntimeException {}
