<?php

namespace Drupal\loop_workers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\loop_workers\Annotation\LoopRate;
use Drupal\loop_workers\Plugin\LoopRate\LoopRateInterface;

/**
 * Manages discovery and instantiation of Loop Rate plugins.
 */
class LoopRateManager extends DefaultPluginManager {

  /**
   * Constructs a new LoopRateManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/LoopRate',
      $namespaces,
      $module_handler,
      LoopRateInterface::class,
      LoopRate::class
    );

    $this->alterInfo('loop_rate_info');
    $this->setCacheBackend($cache_backend, 'loop_rate_plugins');
  }

}
