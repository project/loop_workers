<?php

namespace Drupal\loop_workers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface;
use Drupal\loop_workers\Exception\SuspendLoopException;
use Drupal\ultimate_cron\CronJobInterface;

/**
 * Processes loop items.
 */
class LoopRunner {

  /**
   * The loop worker manager.
   *
   * @var \Drupal\loop_workers\LoopWorkerManager
   */
  protected $loopWorkerManager;

  /**
   * The state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Creates a LoopRunner instance.
   *
   * @param \Drupal\loop_workers\LoopWorkerManager $loop_worker_manager
   *   The loop worker manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger channel.
   */
  public function __construct(
    LoopWorkerManager $loop_worker_manager,
    StateInterface $state,
    CacheBackendInterface $cache_backend,
    LoggerChannelInterface $logger_channel
  ) {
    $this->loopWorkerManager = $loop_worker_manager;
    $this->state = $state;
    $this->cache = $cache_backend;
    $this->loggerChannel = $logger_channel;
  }

  /**
   * Runs one loop.
   *
   * The number of items run is determinded by the loop worker plugin's
   * definition's 'rate' property.
   *
   * @param string $loop_worker_id
   *   The ID of a loop worker plugin.
   */
  public function runLoopWorker(string $loop_worker_id) {
    $loop_worker = $this->loopWorkerManager->createInstance($loop_worker_id);

    // Get the items to run on this iteration.
    $run_item_list = $this->getRunItemList($loop_worker);

    $last_run_id = NULL;
    foreach ($run_item_list as $id => $item) {
      try {
        $loop_worker->processItem($item);
      }
      catch (SuspendLoopException $e) {
        $this->loggerChannel->error(sprintf('Suspended processing loop %s because of exception with message: ', $loop_worker_id, $e->getMessage()));

        // Stop processing this loop.
        break;
      }
      catch (\Exception $e) {
        $this->loggerChannel->error(sprintf('Got exception processing loop %s item %s with message: ', $loop_worker_id, $id, $e->getMessage()));
      }

      // Assign the ID we'll store at the end here, in case the next item
      // fails.
      $last_run_id = $id;
    }

    if ($last_run_id) {
      $this->storeCurrentRunFinalItemId($loop_worker, $last_run_id);
    }
  }

  /**
   * Runs one loop from an Ultimate Cron job entity.
   *
   * @param \Drupal\ultimate_cron\CronJobInterface $ultimate_cron_job
   *   The ultimate_cron job entity.
   */
  public function ultimateCronCallback(CronJobInterface $ultimate_cron_job) {
    $loop_worker_plugin_id = str_replace('loop_worker_', '', $ultimate_cron_job->id());

    $this->runLoopWorker($loop_worker_plugin_id);
  }

  /**
   * Gets the items to run for a worker in one iteration.
   *
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   *
   * @return array
   *   An array of items, keyed by a fixed ID.
   */
  protected function getRunItemList(LoopWorkerInterface $loop_worker): array {
    $total_loop_size = $this->getTotalLoopSize($loop_worker);
    $current_run_size = $this->getRunSize($total_loop_size, $loop_worker);

    $last_run_item_id = $this->getLastRunItemId($loop_worker);

    if ($loop_worker->providesCompleteLoopList()) {
      // If the worker provides the complete loop list, we handle getting the
      // next slice from it.
      $complete_loop_item_list = $this->getLoopItemList($loop_worker);

      // Find the index of the last run item.
      $complete_loop_ids = array_keys($complete_loop_item_list);

      if (is_null($last_run_item_id)) {
        $next_run_item_index = 0;
      }
      else {
        $last_run_item_index = array_search($last_run_item_id, $complete_loop_ids, TRUE);
        $next_run_item_index = $last_run_item_index + 1;
      }

      // Slice the array.
      $run_item_list = array_slice($complete_loop_item_list, $next_run_item_index, $current_run_size, TRUE);

      // We might be too near the end to get the whole amount. Get some more
      // from the start of the list.
      if (count($run_item_list) < $current_run_size) {
        $missing_count = $current_run_size - count($run_item_list);

        $append_item_list = array_slice($complete_loop_item_list, 0, $missing_count, TRUE);

        // Don't use array_merge() as if the keys are numeric it will renumber
        // them.
        foreach ($append_item_list as $key => $item) {
          $run_item_list[$key] = $item;
        }
      }
    }
    else {
      // Get incremental query from the worker.
      $run_item_list = $loop_worker->getRunItemList($last_run_item_id, $current_run_size);

      // We might be too near the end to get the whole amount. Get some more
      // from the start of the list.
      if (count($run_item_list) < $current_run_size) {
        $missing_count = $current_run_size - count($run_item_list);

        $append_item_list = $loop_worker->getRunItemList(NULL, $missing_count);

        // Don't use array_merge() as that will renumber numeric keys.
        foreach ($append_item_list as $key => $item) {
          $run_item_list[$key] = $item;
        }
      }
    }

    return $run_item_list;
  }

  /**
   * Determines the total size of a loop.
   *
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   *
   * @return int
   *   The size of the loop.
   */
  protected function getTotalLoopSize(LoopWorkerInterface $loop_worker): int {
    if ($loop_worker->providesCompleteLoopList()) {
      // If the loop worker provides the complete list, we can just get that and
      // count it. The list is cached, and we'll need it anyway in a moment.
      return count($this->getLoopItemList($loop_worker));
    }
    else {
      // For loop workers that don't provide a complete list, we cache the
      // count, since we can't cache the list.
      $cid = 'loop_workers_count:' . $loop_worker->getPluginId();

      if ($cached = $this->cache->get($cid)) {
        return $cached->data;
      }

      $count = $loop_worker->getLoopItemListCount();

      $this->cache->set($cid, $count, CacheBackendInterface::CACHE_PERMANENT, $loop_worker->getItemListCacheTags());

      return $count;
    }
  }

  /**
   * Gets the number of items to process in one run from the rate plugin.
   *
   * @param int $total_loop_size
   *   The total loop size.
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   *
   * @return int
   *   The number of items to process.
   */
  protected function getRunSize(int $total_loop_size, LoopWorkerInterface $loop_worker): int {
    $loop_rate_plugin = $loop_worker->getRatePlugin();

    return $loop_rate_plugin->getRunSize($total_loop_size, $loop_worker);
  }

  /**
   * Gets the item list for a worker.
   *
   * This is retrieved from the cache. Worker plugins provide the list and the
   * cache tags.
   *
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   *
   * @return array
   *   The array of items the plugin works over.
   */
  protected function getLoopItemList(LoopWorkerInterface $loop_worker): array {
    $cid = 'loop_workers_list:' . $loop_worker->getPluginId();

    if ($cached = $this->cache->get($cid)) {
      return $cached->data;
    }

    $loop_item_list = $loop_worker->getLoopItemList();

    $this->cache->set($cid, $loop_item_list, CacheBackendInterface::CACHE_PERMANENT, $loop_worker->getItemListCacheTags());

    return $loop_item_list;
  }

  /**
   * Gets the ID of the last item in the loop that was processed.
   *
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   *
   * @return mixed
   *   The item ID, or NULL if no processing has been done yet.
   */
  protected function getLastRunItemId(LoopWorkerInterface $loop_worker): mixed {
    return $this->state->get('loop_workers_last_run.' . $loop_worker->getPluginId(), NULL);
  }

  /**
   * Stores the ID of the last item in the loop that was processed.
   *
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   * @param mixed $item_id
   *   The item ID.
   */
  protected function storeCurrentRunFinalItemId(LoopWorkerInterface $loop_worker, mixed $item_id) {
    $this->state->set('loop_workers_last_run.' . $loop_worker->getPluginId(), $item_id);
  }

}
