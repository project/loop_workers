<?php

namespace Drupal\loop_workers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\loop_workers\Annotation\LoopWorker;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface;

/**
 * Manages discovery and instantiation of Loop Worker plugins.
 */
class LoopWorkerManager extends DefaultPluginManager {

  /**
   * Constructs a new LoopWorkerManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/LoopWorker',
      $namespaces,
      $module_handler,
      LoopWorkerInterface::class,
      LoopWorker::class
    );

    $this->alterInfo('loop_worker_info');
    $this->setCacheBackend($cache_backend, 'loop_worker_plugins');
  }

  /**
   * Gets plugin definitions for a specific runner.
   *
   * @param string $runner
   *   The runner. This should match a value in the 'runner' annotation of
   *   loop worker plugins.
   *
   * @return array
   *   An array of plugin definitions.
   */
  public function getPluginDefinitionsByRunner(string $runner): array {
    $filtered_definitions = [];
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($definition['runner'] === $runner) {
        $filtered_definitions[$plugin_id] = $definition;
      }
    }

    return $filtered_definitions;
  }

}
