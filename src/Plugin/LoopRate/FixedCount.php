<?php

namespace Drupal\loop_workers\Plugin\LoopRate;

use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface;

/**
 * Rate plugin which runs a fixed number of items per run.
 *
 * @LoopRate(
 *   id = "fixed_count",
 *   label = @Translation("Fixed count"),
 * )
 */
class FixedCount extends LoopRateBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'count' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRunSize(int $total_loop_size, LoopWorkerInterface $loop_worker): int {
    if (empty($this->configuration['count'])) {
      throw new \LogicException("The fixed_count plugin must have the 'count' configuration key set.");
    }

    return $this->configuration['count'];
  }

}
