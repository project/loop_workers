<?php

namespace Drupal\loop_workers\Plugin\LoopRate;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface;
use Drupal\loop_workers\UltimateCronJobDiscovery;
use Drupal\ultimate_cron\CronRule;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Rate plugin which runs the whole loop over a given interval.
 *
 * @LoopRate(
 *   id = "fixed_interval",
 *   label = @Translation("Fixed interval"),
 * )
 */
class FixedInterval extends LoopRateBase implements ContainerFactoryPluginInterface {

  /**
   * The ultimate cron job discovery service.
   *
   * @var \Drupal\loop_workers\UltimateCronJobDiscovery
   */
  protected $ultimateCronJobDiscovery;

  /**
   * Creates a FixedInterval instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\loop_workers\UltimateCronJobDiscovery $ultimate_cron_job_discovery
   *   The ultimate cron job discovery service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UltimateCronJobDiscovery $ultimate_cron_job_discovery
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->ultimateCronJobDiscovery = $ultimate_cron_job_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('loop_workers.ultimate_cron_job_discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The interval over which the whole loop should be processed. This must
      // be an interval specification as understood by
      // DateInterval::__construct().
      'interval' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRunSize(int $total_loop_size, LoopWorkerInterface $loop_worker): int {
    // @todo support hook_cron runner too.
    if (!$loop_worker->getPluginDefinition()['runner'] == 'ultimate_cron') {
      throw new \LogicException("The fixed_interval rate plugin only supports the ultimate_cron runner.");
    }

    if (!$cron_job = $this->ultimateCronJobDiscovery->loadUltimateCronJob($loop_worker->getPluginId())) {
      throw new \LogicException(sprintf("No ultimate_cron_job entity found for worker plugin %s", $loop_worker->getPluginId()));
    }

    $cron_job_scheduler_configuration = $cron_job->getSchedulerId();
    if (!isset($cron_job_scheduler_configuration['configuration']['rules'][0])) {
      throw new \LogicException(sprintf("The ultimate_cron_job entity for worker plugin %s has no crontab scheduler rule.", $loop_worker->getPluginId()));
    }

    // Try to determine the cron run interval from the cron job entity's
    // scheduler configuration.
    $crontab_rule = $cron_job->getSchedulerId()['configuration']['rules'][0];

    $cron = CronRule::factory($crontab_rule);
    $cron_run_interval = $cron->getNextSchedule() - $cron->getLastSchedule();

    try {
      $total_loop_interval_object = new \DateInterval($this->configuration['interval']);
    }
    catch (\Exception $e) {
      throw new \LogicException(sprintf("The interval '%s' specified for the fixed_interval rate plugin by loop worker plugin %s is not valid.", $this->configuration['interval'], $loop_worker->getPluginId()));
    }

    $total_loop_interval = date_create('@0')->add($total_loop_interval_object)->getTimestamp();

    // Determine how many cron runs will happen in the loop's configured
    // interval. Round down, as a cron run only happens at the end of its
    // interval!
    $number_of_cron_runs = floor($total_loop_interval / $cron_run_interval);

    // Divide the total number of loop items among the cron runs. Round up so
    // we process everything and maybe a bit more rather than not enough.
    $number_of_items_per_cron_run = ceil($total_loop_size / $number_of_cron_runs);

    return $number_of_items_per_cron_run;
  }

}
