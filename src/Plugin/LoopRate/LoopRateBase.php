<?php

namespace Drupal\loop_workers\Plugin\LoopRate;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Loop Rate plugins.
 */
abstract class LoopRateBase extends PluginBase implements LoopRateInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

}
