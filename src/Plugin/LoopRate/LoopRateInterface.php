<?php

namespace Drupal\loop_workers\Plugin\LoopRate;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface;

/**
 * Interface for Loop Rate plugins.
 */
interface LoopRateInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Gets the run count for the loop.
   *
   * @param int $total_loop_size
   *   The total number of items in the loop.
   * @param \Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface $loop_worker
   *   The loop worker plugin.
   *
   * @return int
   *   The number of items to return for a single run.
   */
  public function getRunSize(int $total_loop_size, LoopWorkerInterface $loop_worker): int;

}
