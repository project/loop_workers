<?php

namespace Drupal\loop_workers\Plugin\LoopWorker;

/**
 * Interface for Loop Worker plugins which query the whole loop list in one go.
 *
 * This should be used unless the complete list is too large to hold in memory.
 *
 * @see \Drupal\loop_workers\Plugin\LoopWorker\PartialLoopListInterface
 */
interface CompleteLoopListInterface {

  /**
   * Gets the complete list of items a plugin works over.
   *
   * This list is cached by the LoopRunner; see getItemListCacheTags().
   *
   * @return array
   *   An array of items, keyed by the item ID. This array must have the
   *   following characteristics:
   *    - IDs must be fixed, that is, an ID must always refer to the same item
   *      on each call to this method. (This means that the result of an entity
   *      query may not be used as returned from the query directly, as the keys
   *      are revision IDs.)
   *    - The order of the items must be consistent on each call to this method,
   *      that is, two items must always have the same relative order, although
   *      items may be added and removed between them.
   */
  public function getLoopItemList(): array;

}
