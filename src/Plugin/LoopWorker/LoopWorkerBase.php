<?php

namespace Drupal\loop_workers\Plugin\LoopWorker;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\loop_workers\Plugin\LoopRate\LoopRateInterface;

/**
 * Base class for Loop Worker plugins.
 */
abstract class LoopWorkerBase extends PluginBase implements LoopWorkerInterface {

  /**
   * {@inheritdoc}
   */
  public function providesCompleteLoopList(): bool {
    return ($this instanceof CompleteLoopListInterface);
  }

  /**
   * {@inheritdoc}
   */
  public function getItemListCacheTags(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRatePlugin(): LoopRateInterface {
    $loop_rate_id = $this->pluginDefinition['rate']['type'];
    $loop_rate_configuration = $this->pluginDefinition['rate'];
    unset($loop_rate_configuration['type']);

    try {
      $loop_rate_plugin = \Drupal::service('plugin.manager.loop_rate')->createInstance($loop_rate_id, $loop_rate_configuration);
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException(sprintf("Loop worker plugin '%s' defines as its rate plugin '%s' which does not exist", $this->pluginId, $loop_rate_id));
    }

    return $loop_rate_plugin;
  }

}
