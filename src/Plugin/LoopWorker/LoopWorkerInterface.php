<?php

namespace Drupal\loop_workers\Plugin\LoopWorker;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\loop_workers\Plugin\LoopRate\LoopRateInterface;

/**
 * Interface for Loop Worker plugins.
 *
 * Plugins must additionally implement one of:
 *  - PartialLoopListInterface
 *  - CompleteLoopListInterface
 */
interface LoopWorkerInterface extends PluginInspectionInterface {

  /**
   * Whether the loop worker provides a complete or partial list.
   *
   * @return bool
   *   TRUE if this plugin provides a complete list; FALSE if it does not.
   */
  public function providesCompleteLoopList(): bool;

  /**
   * Gets the cache tags for the list of items.
   *
   * These are used by the LoopRunner to cache the complete list and the count
   * of items.
   *
   * @return array
   *   The list of cache tags.
   */
  public function getItemListCacheTags(): array;

  /**
   * Gets the rate plugin for this worker plugin.
   *
   * @return \Drupal\loop_workers\Plugin\LoopRate\LoopRateInterface
   *   The rate plugin, configured with values from the current loop worker
   *   plugin's "rate" annotation property.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Throws an exception if the definition of this plugin doesn't properly
   *   specify a loop rate plugin ID.
   */
  public function getRatePlugin(): LoopRateInterface;

  /**
   * Processes a single item.
   *
   * @param mixed $item
   *   The item to process. This is an item from the array returned by either
   *   getLoopItemList() or getRunItemList().
   *
   * @throws \Drupal\loop_workers\Exception\SuspendLoopException
   *   Throws a SuspendLoopException to indicate that no further items should be
   *   processed in this run.
   * @throws \Exception
   *   Throws an exception if there is a general problem processing the item.
   */
  public function processItem(mixed $item);

}
