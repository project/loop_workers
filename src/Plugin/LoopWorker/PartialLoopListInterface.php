<?php

namespace Drupal\loop_workers\Plugin\LoopWorker;

/**
 * Interface for Loop Worker plugins which query the loop list incrementally.
 *
 * This should be used when the list of items is so large that it would risk
 * exhausting memory.
 *
 * @see \Drupal\loop_workers\Plugin\LoopWorker\CompleteLoopListInterface
 */
interface PartialLoopListInterface {

  /**
   * Gets the count of items a plugin works over.
   *
   * This result is cached by the LoopRunner; see getItemListCacheTags().
   *
   * @return int
   *   The count of items.
   */
  public function getLoopItemListCount(): int;

  /**
   * Gets the list of items for the plugin to work over in one run.
   *
   * Implementations do not need to concern themselves with looping round at the
   * end, and should simply return a partial run from the end of the loop list.
   * The LoopRunner will call the method a second time with $last_run_item_id
   * set to NULL to get the more items from the start of the loop.
   *
   * @param mixed $last_run_item_id
   *   The ID of the most recently processed item, or NULL if the loop has not
   *   been run yet.
   * @param int $current_run_size
   *   The number of items that this method should return.
   *
   * @return array
   *   An array of items, keyed by the item ID. This array must have the
   *   following characteristics:
   *    - IDs must be fixed, that is, an ID must always refer to the same item
   *      on each call to this method. (This means that the result of an entity
   *      query may not be used as returned from the query directly, as the keys
   *      are revision IDs.)
   *    - The order of the items must be consistent on each call to this method,
   *      that is, two items must always have the same relative order, although
   *      items may be added and removed between them.
   */
  public function getRunItemList(mixed $last_run_item_id, int $current_run_size): array;

}
