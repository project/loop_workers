<?php

namespace Drupal\loop_workers;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\ultimate_cron\CronJobInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Ensures ultimate cron jobs exist for loops which declare it as runner.
 */
class UltimateCronJobDiscovery {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The loop worker manager.
   *
   * @var \Drupal\loop_workers\LoopWorkerManager
   */
  protected $loopWorkerManager;

  /**
   * Creates a UltimateCronJobDiscovery instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\loop_workers\LoopWorkerManager $loop_worker_manager
   *   The loop worker manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoopWorkerManager $loop_worker_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->loopWorkerManager = $loop_worker_manager;
  }

  /**
   * Loads the ultimate cron job entity for a loop worker, if one exists.
   *
   * @param string $worker_plugin_id
   *   The worker plugin ID.
   *
   * @return \Drupal\ultimate_cron\CronJobInterface|null
   *   The cron job entity, or NULL if none exists.
   */
  public function loadUltimateCronJob(string $worker_plugin_id): ?CronJobInterface {
    $job_id = 'loop_worker_' . str_replace(':', '__', $worker_plugin_id);

    $ultimate_cron_job_entity_storage = $this->entityTypeManager->getStorage('ultimate_cron_job');
    return $ultimate_cron_job_entity_storage->load($job_id);
  }

  /**
   * Ensures ultimate cron jobs exist for loops which declare it as runner.
   */
  public function discoverCronJobs() {
    $ultimate_cron_job_entity_storage = $this->entityTypeManager->getStorage('ultimate_cron_job');

    foreach ($this->loopWorkerManager->getPluginDefinitionsByRunner('ultimate_cron') as $worker_plugin_id => $worker_plugin_definition) {
      if (!$this->loadUltimateCronJob($worker_plugin_id)) {
        $values = [
          'title' => $this->t('Loop: @title', ['@title' => $worker_plugin_definition['label']]),
          'id' => $job_id,
          'module' => $worker_plugin_definition['provider'],
          // Process queue jobs later by default.
          'weight' => 20,
          'callback' => 'loop_workers.loop_runner:ultimateCronCallback',
          'scheduler' => [
            'id' => 'simple',
            'configuration' => [
              'rules' => ['* * * * *'],
            ],
          ],
        ];

        $job = $ultimate_cron_job_entity_storage->create($values);

        $job->save();
      }
    }
  }

}
