<?php

namespace Drupal\loop_worker_kernel_test\Plugin\LoopWorker;

use Drupal\loop_workers\Plugin\LoopWorker\CompleteLoopListInterface;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerBase;

/**
 * Test plugin for a full query loop.
 *
 * @LoopWorker(
 *   id = "weekdays",
 *   label = @Translation("Weekdays"),
 *   rate = {
 *     "type" = "fixed_count",
 *     "count" = 5,
 *   },
 * )
 */
class FullList extends LoopWorkerBase implements CompleteLoopListInterface {

  /**
   * {@inheritdoc}
   */
  public function getLoopItemList(): array {
    return [
      // Can just use numeric keys, as the list never changes.
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $item) {
    $progress = \Drupal::state()->get('loop_worker_kernel_test_weekdays', []);

    $progress[] = $item;

    \Drupal::state()->set('loop_worker_kernel_test_weekdays', $progress);
  }

}
