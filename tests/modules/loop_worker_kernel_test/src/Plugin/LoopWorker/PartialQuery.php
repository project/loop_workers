<?php

namespace Drupal\loop_worker_kernel_test\Plugin\LoopWorker;

use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerBase;
use Drupal\loop_workers\Plugin\LoopWorker\PartialLoopListInterface;

/**
 * Test plugin for a partial query loop.
 *
 * @LoopWorker(
 *   id = "partial_query",
 *   label = @Translation("Partial query"),
 *   rate = {
 *     "type" = "fixed_count",
 *     "count" = 5,
 *   },
 * )
 */
class PartialQuery extends LoopWorkerBase implements PartialLoopListInterface {

  /**
   * {@inheritdoc}
   */
  public function getLoopItemListCount(): int {
    $query = \Drupal::service('database')->select('loop_items', 'l');
    $num_rows = $query->countQuery()->execute()->fetchField();

    return $num_rows;
  }

  /**
   * {@inheritdoc}
   */
  public function getRunItemList(mixed $last_run_item_id, int $current_run_size): array {
    $query = \Drupal::service('database')->select('loop_items', 'l');
    $query->fields('l', ['id', 'data']);

    if (!is_null($last_run_item_id)) {
      $query->condition('l.id', $last_run_item_id, '>');
    }

    $query->orderBy('id', 'ASC')
      ->range(0, $current_run_size);
    $result = $query->execute();

    return $result->fetchAllKeyed();
  }

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $item) {
    $progress = \Drupal::state()->get('loop_worker_kernel_test_partial_query', []);

    $progress[] = $item;

    \Drupal::state()->set('loop_worker_kernel_test_partial_query', $progress);
  }

}
