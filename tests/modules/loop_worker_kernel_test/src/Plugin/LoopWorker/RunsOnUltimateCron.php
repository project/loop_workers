<?php

namespace Drupal\loop_worker_kernel_test\Plugin\LoopWorker;

use Drupal\loop_workers\Plugin\LoopWorker\CompleteLoopListInterface;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerBase;

/**
 * Loop worker which runs with Ultimate Cron.
 *
 * @LoopWorker(
 *   id = "runs_on_ultimate_cron",
 *   label = @Translation("Runs on Ultimate Cron"),
 *   runner = "ultimate_cron",
 *   rate = {
 *     "type" = "fixed_count",
 *     "count" = 1,
 *   },
 * )
 */
class RunsOnUltimateCron extends LoopWorkerBase implements CompleteLoopListInterface {

  /**
   * {@inheritdoc}
   */
  public function getLoopItemList(): array {
    return [
      1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $item) {
    \Drupal::state()->set('runs_on_ultimate_cron', TRUE);
  }

}
