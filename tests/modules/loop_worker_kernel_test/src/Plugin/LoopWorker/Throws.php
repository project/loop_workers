<?php

namespace Drupal\loop_worker_kernel_test\Plugin\LoopWorker;

use Drupal\loop_workers\Plugin\LoopWorker\CompleteLoopListInterface;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerBase;
use Drupal\loop_workers\Exception\SuspendLoopException;

/**
 * Test plugin which throws exceptions.
 *
 * @LoopWorker(
 *   id = "throws",
 *   label = @Translation("Throws"),
 *   rate = {
 *     "type" = "fixed_count",
 *     "count" = 5,
 *   },
 * )
 */
class Throws extends LoopWorkerBase implements CompleteLoopListInterface {

  /**
   * {@inheritdoc}
   */
  public function getLoopItemList(): array {
    return [
      1 => 1,
      2 => 2,
      // Won't get processed, but the next items will.
      3 => 'throw',
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      // Won't get processed, and will stop processing of the next item too.
      8 => 'suspend',
      9 => 9,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $item) {
    $progress = \Drupal::state()->get('loop_worker_kernel_test_throws', []);

    if ($item === 'throw') {
      // Update the state without setting the item we've not processed.
      \Drupal::state()->set('loop_worker_kernel_test_throws', $progress);

      throw new \Exception('throw');
    }
    if ($item === 'suspend') {
      // Update the state without setting the item we've not processed.
      \Drupal::state()->set('loop_worker_kernel_test_throws', $progress);

      throw new SuspendLoopException('suspend');
    }

    $progress[] = $item;

    \Drupal::state()->set('loop_worker_kernel_test_throws', $progress);
  }

}
