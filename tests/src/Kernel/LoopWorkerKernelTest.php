<?php

namespace Drupal\Tests\loop_workers\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test loop workers.
 *
 * @group loop_workers
 */
class LoopWorkerKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'loop_workers',
    'loop_worker_kernel_test',
  ];

  /**
   * The cron service.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->cron = $this->container->get('cron');
  }

  /**
   * Tests a complete list loop worker plugin.
   */
  public function testCompleteListLoop() {
    $loop_runner = $this->container->get('loop_workers.loop_runner');
    $loop_runner->runLoopWorker('weekdays');

    $this->assertEquals([
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
    ], \Drupal::state()->get('loop_worker_kernel_test_weekdays'));

    \Drupal::state()->set('loop_worker_kernel_test_weekdays', []);

    $loop_runner->runLoopWorker('weekdays');
    $this->assertEquals([
      'Saturday',
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
    ], \Drupal::state()->get('loop_worker_kernel_test_weekdays'));
  }

  /**
   * Tests a partial list loop worker plugin.
   */
  public function testPartialListLoop() {
    $this->installSchema('loop_worker_kernel_test', ['loop_items']);
    $query = $this->container
      ->get('database')
      ->insert('loop_items')
      ->fields(['id', 'data']);
    foreach (range(1, 10) as $index) {
      $query->values([
        'id' => $index,
        'data' => $index,
      ]);
    }
    $query->execute();

    $loop_runner = $this->container->get('loop_workers.loop_runner');

    $loop_runner->runLoopWorker('partial_query');
    $this->assertEquals(range(1, 5), \Drupal::state()->get('loop_worker_kernel_test_partial_query'));

    \Drupal::state()->set('loop_worker_kernel_test_partial_query', []);

    $loop_runner->runLoopWorker('partial_query');
    $this->assertEquals(range(6, 10), \Drupal::state()->get('loop_worker_kernel_test_partial_query'));

    \Drupal::state()->set('loop_worker_kernel_test_partial_query', []);

    // Now delete an item.
    $this->container->get('database')->delete('loop_items')
      ->condition('id', 4)
      ->execute();

    $loop_runner->runLoopWorker('partial_query');
    $this->assertEquals([1, 2, 3, 5, 6], \Drupal::state()->get('loop_worker_kernel_test_partial_query'));

    \Drupal::state()->set('loop_worker_kernel_test_partial_query', []);

    $loop_runner->runLoopWorker('partial_query');
    $this->assertEquals([7, 8, 9, 10, 1], \Drupal::state()->get('loop_worker_kernel_test_partial_query'));
  }

  /**
   * Tests exceptions thrown by a loop worker plugin.
   */
  public function testExceptions() {
    $loop_runner = $this->container->get('loop_workers.loop_runner');

    $loop_runner->runLoopWorker('throws');
    $this->assertEquals([1, 2, 4, 5], \Drupal::state()->get('loop_worker_kernel_test_throws'));

    \Drupal::state()->set('loop_worker_kernel_test_throws', []);

    $loop_runner->runLoopWorker('throws');
    $this->assertEquals([6, 7], \Drupal::state()->get('loop_worker_kernel_test_throws'));
  }

  // Something cached.
}
