<?php

namespace Drupal\Tests\loop_workers\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests integration with Ultimate Cron module.
 *
 * @group loop_workers
 */
class UltimateCronKernelTest extends KernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'loop_workers',
    'loop_worker_kernel_test',
    'ultimate_cron',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->installSchema(
      'ultimate_cron',
      ['ultimate_cron_log', 'ultimate_cron_lock']
    );
  }

  /**
   * Tests a loop run by ultimate cron.
   */
  public function testUltimateCron() {
    // Ensure the ultimate_cron job exists for the loop worker.
    loop_workers_rebuild();

    // With ultimate_cron enabled, the cron service is replaced, so running that
    // runs ultimate_cron jobs only.
    $cron = $this->container->get('cron');
    $cron->run();

    $this->assertEquals(TRUE, \Drupal::state()->get('runs_on_ultimate_cron'));
    $this->assertEquals(NULL, \Drupal::state()->get('runs_on_hook_cron'));

    // Uninstall ultimate_cron to run cron again.
    $this->container->get('module_installer')->uninstall(['ultimate_cron']);
    $this->container->get('kernel')->rebuildContainer();

    \Drupal::state()->set('runs_on_ultimate_cron', NULL);
    \Drupal::state()->set('runs_on_hook_cron', NULL);

    $cron = $this->container->get('cron');
    $cron->run();

    // Without ultimate_cron, only hook_cron() loops are run.
    $this->assertEquals(TRUE, \Drupal::state()->get('runs_on_hook_cron'));
    $this->assertEquals(NULL, \Drupal::state()->get('runs_on_ultimate_cron'));
  }

}
