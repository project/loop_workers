<?php

namespace Drupal\Tests\loop_workers\Unit;

use Drupal\loop_workers\Plugin\LoopRate\FixedInterval;
use Drupal\loop_workers\Plugin\LoopWorker\LoopWorkerInterface;
use Drupal\loop_workers\UltimateCronJobDiscovery;
use Drupal\Tests\UnitTestCase;
use Drupal\ultimate_cron\CronJobInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the fixed_interval rate plugin.
 *
 * @group loop_workers
 */
class FixedIntervalLoopRateTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * {@inheritdoc}
   */
  public static function setUpBeforeClass(): void {
    // Mocking CronJobInterface causes a crash because of these constants not
    // being defined, presumably because the module file they're defined in
    // isn't loaded.
    define('ULTIMATE_CRON_LOG_TYPE_NORMAL', '');
    define('ULTIMATE_CRON_LOG_TYPE_ALL', '');
  }

  /**
   * Data provider for testFixedIntervalRatePlugin().
   */
  public function dataFixedIntervalRatePlugin() {
    return [
      'hour-day-24' => [
        // Crontab.
        '0 * * * *',
        // Loop period configuration for the fixed_interval plugin.
        'P1D',
        // Loop size.
        24,
        // Expected run size.
        1,
      ],
      'hour-day-50' => [
        '0 * * * *',
        'P1D',
        50,
        3,
      ],
      '5mins-day-1000' => [
        '*/5 * * * *',
        'P1D',
        1000,
        4,
      ],
    ];
  }

  /**
   * Tests the fixed_interval rate plugin.
   *
   * @param string $crontab
   *   The crontab string to use for a mocked ultimate_cron_job entity.
   * @param string $plugin_interval
   *   The loop interval to set on the fixed_interval plugin, as a DateInterval
   *   format string.
   * @param int $loop_size
   *   The size of the whole loop.
   * @param int $expected_run_size
   *   The expected number of items to process in each run.
   *
   * @dataProvider dataFixedIntervalRatePlugin
   */
  public function testFixedIntervalRatePlugin(string $crontab, string $plugin_interval, int $loop_size, int $expected_run_size) {
    $cron_job = $this->prophesize(CronJobInterface::class);
    $cron_job->getSchedulerId()->willReturn([
      'configuration' => [
        'rules' => [
          0 => $crontab,
        ],
      ],
    ]);

    $ultimate_cron_job_discovery = $this->prophesize(UltimateCronJobDiscovery::class);
    $ultimate_cron_job_discovery->loadUltimateCronJob('worker')->willReturn($cron_job->reveal());

    // Construct with dummy values, as the plugin doesn't use these.
    $fixed_interval = new FixedInterval([], 'fixed_interval', [], $ultimate_cron_job_discovery->reveal());
    $fixed_interval->setConfiguration(['interval' => $plugin_interval]);

    $loop_worker = $this->prophesize(LoopWorkerInterface::class);
    $loop_worker->getPluginDefinition()->willReturn([
      'runner' => 'ultimate_cron',
    ]);
    $loop_worker->getPluginId()->willReturn('worker');

    $calculated_run_size = $fixed_interval->getRunSize($loop_size, $loop_worker->reveal());

    $this->assertEquals($expected_run_size, $calculated_run_size);
  }

}
